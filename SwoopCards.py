# -*- coding: utf-8 -*-
#import all dependencies (if maked with [#] above, need to be installed via pip installer)
from tkinter import *
import sqlite3
import sys
from random import *
#[#]
from PIL import Image, ImageTk
from tkinter.ttk import Progressbar
from decimal import Decimal
#[#]
from pygame import mixer
import ctypes
#Code below creates a window, and mute button
#some variables are also set to 0, and a table is created
win = Tk()
win.overrideredirect(True)
superwin = Frame(win, bg="#3e3e3e", height="80", width="200")
superwin.pack(side=TOP)
#changes window size based on screensize
user32 = ctypes.windll.user32
screensize = user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)
print(screensize)
win.geometry(str(screensize[0])+"x"+str(screensize[1]))
win.configure(bg="#3e3e3e")
win.title('SwoopCards Release 1')
player1login = 0
player2login = 0
try: win.iconbitmap('swoop.ico')
except TclError: pass
db = sqlite3.connect('swoopData.db')
c = db.cursor()
c.execute("""CREATE TABLE IF NOT EXISTS loginDatabase(username BLOB, password BLOB, highscore INTEGER,
 totalscore INTEGER, games_played INTEGER, rank INTEGER, experience INTEGER, winstreak INTEGER, losingstreak INTEGER)""")
player1roundsWon = 0
player2roundsWon = 0
userSort = 0
highSort = 0
totalSort = 0
musicStop = 0
muted = 0
winnerDisplay = []
player1Cardlist = []
player2Cardlist = []
themeStyle = "light"
#linux support
if sys.platform.startswith("linux"):
    systemFont = "symbol"
else:
    systemFont = "System"
#starting music
mixer.init()
mixer.music.load("load.mp3")
mixer.music.play(-1)
#on mute button press
def muteKey():
    global muted
    if muted == 0:
        mixer.music.pause()
        muted = 1
    else:
        mixer.music.unpause()
        muted = 0
#mute button image loaded
loadMute = Image.open("muteicon.png")
loadMute = loadMute.resize((30, 30), Image.ANTIALIAS)
renderMuteImg = ImageTk.PhotoImage(loadMute)
muteImg = Button(superwin, image=renderMuteImg, bg="#3E3E3E", command=muteKey, relief=FLAT)
muteImg.image = renderMuteImg
#logging out
def L_Out1():
    global player1login
    player1login = 0
    loginSuccess1.place_forget()
    logOutfromLgn1bt.place_forget()
    loginSuccess1.configure(text='Logged out successfully.')
    loginSuccess1.place(relx=0.25, rely=0.7, anchor=CENTER)
def L_Out2():
    global player2login
    player2login = 0
    loginSuccess2.place_forget()
    logOutfromLgn2bt.place_forget()
    loginSuccess2.configure(text='Logged out successfully.')
    loginSuccess2.place(relx=0.75, rely=0.7, anchor=CENTER)
#events for greyed out entry fields
def clear_entry(event, registerUsername):
    registerUsername.delete(0, END)
    registerUsername.configure(fg="black")
def clear_entrypass(event, registerPassword):
    registerPassword.delete(0, END)
    registerPassword.configure(fg="black")
    registerPassword.config(show="*")
def clear_user1(event, usernameEntry1):
    usernameEntry1.delete(0, END)
    usernameEntry1.configure(fg="black")
def clear_user2(event, usernameEntry2):
    usernameEntry2.delete(0, END)
    usernameEntry2.configure(fg="black")
def clear_pass1(event, passwordEntry1):
    passwordEntry1.delete(0, END)
    passwordEntry1.configure(fg="black")
    passwordEntry1.config(show="*")
def clear_pass2(event, passwordEntry2):
    passwordEntry2.delete(0, END)
    passwordEntry2.configure(fg="black")
    passwordEntry2.config(show="*")
def new_Pass(event, newpassword):
    newpassword.delete(0, END)
    newpassword.configure(fg="black")
    newpassword.config(show="*")
#a function that hides everything on screen
def hideAll():
    for i in win.winfo_children():
        i.place_forget()
def loadGame():
    # takes you to next screen (main menu)
    hideAll()
    loginQwery.place(relx=0.5, rely=0.2, anchor=CENTER)
    # u1
    usernameEntry1.place(relx=0.25, rely=0.4, anchor=CENTER)
    usernameplaceholder = 'Username'
    usernameEntry1.configure(fg="#696969")
    usernameEntry1.insert(0, usernameplaceholder)
    usernameEntry1.bind("<FocusIn>", lambda event: clear_user1(event, usernameEntry1))
    # p1
    passwordEntry1.place(relx=0.25, rely=0.5, anchor=CENTER)
    passwordplaceholder = 'Password'
    passwordEntry1.configure(fg="#696969")
    passwordEntry1.insert(0, passwordplaceholder)
    passwordEntry1.bind("<FocusIn>", lambda event: clear_pass1(event, passwordEntry1))
    # playerentry
    player1Enter.place(relx=0.25, rely=0.6, anchor=CENTER)
    # u2
    usernameEntry2.place(relx=0.75, rely=0.4, anchor=CENTER)
    usernameEntry2.configure(fg="#696969")
    usernameEntry2.insert(0, usernameplaceholder)
    usernameEntry2.bind("<FocusIn>", lambda event: clear_user2(event, usernameEntry2))
    # p2
    passwordEntry2.place(relx=0.75, rely=0.5, anchor=CENTER)
    passwordEntry2.configure(fg="#696969")
    passwordEntry2.insert(0, passwordplaceholder)
    passwordEntry2.bind("<FocusIn>", lambda event: clear_pass2(event, passwordEntry2))
    # playerentry
    player2Enter.place(relx=0.75, rely=0.6, anchor=CENTER)
    registerUser.place(relx=0.5, rely=0.13, anchor=CENTER)
    enterGame.place(relx=0.5, rely=0.82, anchor=CENTER)
def sysQuitfromMain():
    global musicStop
    # takes you to Quit Confirm
    hideAll()
    confirmLabel.place(relx=0.5, rely=0.4, anchor=CENTER)
    quitConfirmButton.place(relx=0.4, rely=0.55, anchor=CENTER)
    quitReturnMain.place(relx=0.6, rely=0.55, anchor=CENTER)
    musicStop = 1
def sysExit():
    # QUIT PROGRAM
    sys.exit()
def returnMain():
    global musicStop
    # RETURN TO MAIN MENU
    hideAll()
    if musicStop != 1:
        mixer.music.stop()
        mixer.music.load("load.mp3")
        mixer.music.play(-1)
    musicStop = 0
    titleLabel.place(relx=0.5, rely=0.1, anchor=CENTER)
    loadsub = Image.open("SwoopText" + themeStyle.capitalize() + ".png")
    loadsub = loadsub.resize((270, 50), Image.ANTIALIAS)
    rendersub = ImageTk.PhotoImage(loadsub)
    subimg = Label(win, image=rendersub, bg="#3e3e3e")
    subimg.image = rendersub
    subimg.place(relx=0.5, rely=0.2, anchor=CENTER)
    enterButton.place(relx=0.3, rely=0.4, anchor=CENTER)
    quitButton.place(relx=0.7, rely=0.4, anchor=CENTER)
    leaderboardButton.place(relx=0.3, rely=0.8, anchor=CENTER)
    creditButton.place(relx=0.7, rely=0.8, anchor=CENTER)
    muteImg.pack()
#loads credit screen
def creditLoad():
    hideAll()
    mixer.music.stop()
    mixer.music.load("credits.mp3")
    mixer.music.play(-1)
    creditList = ["SwoopCards is owned by SwoopSoft & Co.",
                  "Swoopsoft & Co. Industries under the aliases:",
                  "SwoopSoft",
                  "SwoopCo.",
                  "SwoopSoft Industries",
                  "SwoopSoft & Partners",
                  "SwoopJLW Developers",
                  "SwoopSoft Developer Team",
                  "The source code, assets and all files included in the download",
                  "are covered under the BSD-2 Clause license where:",
                  "• All code can be modified and reproduced alongside recognition.",
                  "• All code can be reproduced without modification alongside recognition.",
                  "• All code may not be sold, under any circumstances.",
                  "• All assets may not be sold, under any circumstances.",
                  "This code was made by the COO of SwoopSoft - Junior Brito",
                  "All code is written by Junior Brito with assistance from",
                  "Luke Williams (lgwilliams)",
                  "All assets belong to Junior Brito, and as extended, SwoopSoft & Co.",
                  "Music published by the Pokémon Company, owned by GameFreak, CapCom and Nintendo©",
                  "Music owned by Lantis Company, Limited (on behalf of Lantis); SOLAR Music Rights",
                  "SWOOPCARDS v0.10"]
    creditList.reverse()
    titleLabel.place(relx=0.5, rely=0.1, anchor=CENTER)
    creditsListbox = Listbox(win, font=systemFont + " 18", bg="#2e2e2e", fg="white",
                                highlightthickness=0, selectmode=SINGLE, selectbackground="#2e2e2e", relief=FLAT,
                                width=80, height=21)
    for i in creditList:
        creditsListbox.insert(1, i)
    creditsListbox.place(relx=0.5, rely=0.5, anchor=CENTER)
    backButton_leaderboard.place(relx=0.5, rely=0.89, anchor=CENTER)
#loads leaderboard screen
def leaderboard_place():
    global stringScore, stringUsers, leaderboardUs_Box, leaderboardSc_Box, leaderboardTSc_Box
    hideAll()
    mixer.music.stop()
    mixer.music.load("leaderboard.mp3")
    mixer.music.play(-1)
    leaderboardTitle.place(relx=0.5, rely=0.1, anchor=CENTER)
    c.execute("SELECT username FROM loginDatabase ORDER BY highscore DESC")
    rowsUser = reversed(c.fetchall())
    rowsUser = [i[0] for i in rowsUser]
    print(rowsUser)
    leaderboardUs_Box = Listbox(win, font=systemFont + " 20", bg="#2e2e2e", fg="white",
                                highlightthickness=0, selectmode=SINGLE, selectbackground="#2e2e2e", relief=FLAT,
                                width=20, height=15)
    for i in rowsUser:
        leaderboardUs_Box.insert(0, i)
    c.execute("SELECT highscore FROM loginDatabase ORDER BY highscore DESC")
    rowsHighscore = reversed(c.fetchall())
    rowsHighscore = [i[0] for i in rowsHighscore]
    print(rowsHighscore)
    leaderboardSc_Box = Listbox(win, font=systemFont + " 20", bg="#2e2e2e", fg="white",
                                highlightthickness=0, selectmode=SINGLE, selectbackground="#2e2e2e", relief=FLAT,
                                width=20, height=15)
    for i in rowsHighscore:
        leaderboardSc_Box.insert(0, i)
    c.execute("SELECT totalscore FROM loginDatabase ORDER BY highscore DESC")
    rowsTotalscore = reversed(c.fetchall())
    rowsTotalscore = [i[0] for i in rowsTotalscore]
    print(rowsTotalscore)
    leaderboardTSc_Box = Listbox(win, font=systemFont + " 20", bg="#2e2e2e", fg="white",
                                 highlightthickness=0, selectmode=SINGLE, selectbackground="#2e2e2e", relief=FLAT,
                                 width=20, height=15)
    for i in rowsTotalscore:
        leaderboardTSc_Box.insert(0, i)
    leaderboardUs_Box.place(relx=0.2024, rely=0.6, anchor=CENTER)
    leaderboardSc_Box.place(relx=0.35, rely=0.6, anchor=CENTER)
    leaderboardTSc_Box.place(relx=0.502, rely=0.6, anchor=CENTER)
    backButton_leaderboard.place(relx=0.8, rely=0.5, anchor=CENTER)
    usernameLabel.place(relx=0.2, rely=0.34, anchor=CENTER)
    highscoreLabel.place(relx=0.35, rely=0.34, anchor=CENTER)
    totalscoreLabel.place(relx=0.5, rely=0.34, anchor=CENTER)
#checks login credentials
def checkCredp1():
    global player1login, usernameVar1, passwordVar1
    if player1login == 0:
        usernameEntry1.focus_set()
        usernameVar1 = usernameEntry1.get()
        passwordEntry1.focus_set()
        passwordVar1 = passwordEntry1.get()
        while True:
            credentialCheck1 = ("SELECT * FROM LoginDatabase WHERE username = ? AND password = ?")
            c.execute(credentialCheck1, [(usernameVar1), (passwordVar1)])
            checkComplete1 = c.fetchall()

            if checkComplete1:
                for i in checkComplete1:
                    loginSuccess1.place_forget()
                    loginSuccess1.configure(text="Successfully logged in as " + usernameVar1)
                    loginSuccess1.place(relx=0.25, rely=0.7, anchor=CENTER)
                    player1login = 1
                    logOutfromLgn1bt.place(relx=0.25, rely=0.78, anchor=CENTER)
                break

            else:
                loginSuccess1.place_forget()
                loginSuccess1.configure(text='Credentials not recognised')
                loginSuccess1.place(relx=0.25, rely=0.7, anchor=CENTER)
            break
    else:
        loginSuccess1.place_forget()
        loginSuccess1.configure(text='ERROR: Already logged in')
        loginSuccess1.place(relx=0.25, rely=0.7, anchor=CENTER)
def checkCredp2():
    global player2login, usernameVar2, passwordVar2
    if player2login == 0:
        usernameEntry2.focus_set()
        usernameVar2 = usernameEntry2.get()
        passwordEntry2.focus_set()
        passwordVar2 = passwordEntry2.get()
        while True:
            credentialCheck2 = ("SELECT * FROM LoginDatabase WHERE username = ? AND password = ?")
            c.execute(credentialCheck2, [(usernameVar2), (passwordVar2)])
            checkComplete2 = c.fetchall()

            if checkComplete2:
                for i in checkComplete2:
                    loginSuccess2.place_forget()
                    loginSuccess2.configure(text="Successfully logged in as " + usernameVar2)
                    loginSuccess2.place(relx=0.75, rely=0.7, anchor=CENTER)
                    player2login = 1
                    logOutfromLgn2bt.place(relx=0.75, rely=0.8, anchor=CENTER)
                break

            else:
                loginSuccess2.place_forget()
                loginSuccess2.configure(text='Credentials not recognised')
                loginSuccess2.place(relx=0.75, rely=0.7, anchor=CENTER)
            break
    else:
        loginSuccess2.place_forget()
        loginSuccess2.configure(text='ERROR: Already logged in')
        loginSuccess2.place(relx=0.75, rely=0.7, anchor=CENTER)
#new user register screen
def registerNew():
    hideAll()
    usernameEntry1.delete(0, END)
    usernameEntry2.delete(0, END)
    passwordEntry1.delete(0, END)
    passwordEntry2.delete(0, END)
    registerConfirm.place(relx=0.2, rely=0.41, anchor=CENTER)
    usernameregister.place(relx=0.2, rely=0.27, anchor=CENTER)
    usernameplaceholder = 'Username'
    usernameregister.configure(fg="#696969")
    usernameregister.insert(0, usernameplaceholder)
    usernameregister.bind("<FocusIn>", lambda event: clear_entry(event, usernameregister))
    passwordregister.place(relx=0.2, rely=0.34, anchor=CENTER)
    passwordplaceholder = 'Password'
    passwordregister.configure(fg="#696969")
    passwordregister.insert(0, passwordplaceholder)
    passwordregister.bind("<FocusIn>", lambda event: clear_entrypass(event, passwordregister))
#checking new credentials
def registerNewuser():
    usernameregister.focus_set()
    username = usernameregister.get()
    passwordregister.focus_set()
    password = passwordregister.get()
    if username in [i[0] for i in getAll()]:
        invalidCredentials.place(relx=0.2, rely=0.5, anchor=CENTER)
        return
    else:
        startRank = 1
        highscore = 0
        invalidCredentials.place_forget()
        c.execute("""INSERT INTO loginDatabase (username, password, highscore, rank, experience, winstreak, losingstreak,
        totalscore, games_played)
                  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)""",
                (username, password, highscore, startRank, highscore, highscore, highscore, highscore, highscore))
        db.commit()
        # user inserts data into relevent columns of the table with the inputted values
    backButton_register.place(relx=0.2, rely=0.5, anchor=CENTER)
#places login screen again
def returnLogin():
    hideAll()
    loginQwery.place(relx=0.5, rely=0.2, anchor=CENTER)
    # u1
    usernameEntry1.place(relx=0.25, rely=0.4, anchor=CENTER)
    usernameplaceholder = 'Username'
    usernameEntry1.configure(fg="#696969")
    usernameEntry1.insert(0, usernameplaceholder)
    usernameEntry1.bind("<FocusIn>", lambda event: clear_user1(event, usernameEntry1))
    # p1
    passwordEntry1.place(relx=0.25, rely=0.5, anchor=CENTER)
    passwordplaceholder = 'Password'
    passwordEntry1.configure(fg="#696969")
    passwordEntry1.insert(0, passwordplaceholder)
    passwordEntry1.bind("<FocusIn>", lambda event: clear_pass1(event, passwordEntry1))
    # playerentry
    player1Enter.place(relx=0.25, rely=0.6, anchor=CENTER)
    # u2
    usernameEntry2.place(relx=0.75, rely=0.4, anchor=CENTER)
    usernameEntry2.configure(fg="#696969")
    usernameEntry2.insert(0, usernameplaceholder)
    usernameEntry2.bind("<FocusIn>", lambda event: clear_user2(event, usernameEntry2))
    # p2
    passwordEntry2.place(relx=0.75, rely=0.5, anchor=CENTER)
    passwordEntry2.configure(fg="#696969")
    passwordEntry2.insert(0, passwordplaceholder)
    passwordEntry2.bind("<FocusIn>", lambda event: clear_pass2(event, passwordEntry2))
    # playerentry
    player2Enter.place(relx=0.75, rely=0.6, anchor=CENTER)
    registerUser.place(relx=0.5, rely=0.13, anchor=CENTER)
    enterGame.place(relx=0.5, rely=0.82, anchor=CENTER)
def getAll():
    """Get the contents of the database"""
    with sqlite3.connect('swoopData.db') as db:
        c = db.cursor()
        return list(c.execute("SELECT * FROM loginDatabase"))
#when players are logged in
def gameStart():
    musicStop = 0
    if player1login == 1 and player2login == 1:
        hideAll()
        titleLabel.place(relx=0.5, rely=0.1, anchor=CENTER)
        classic_modeEntry.place(relx=0.16, rely=0.6, anchor=CENTER)
        settings_modeEntry.place(relx=0.5, rely=0.6, anchor=CENTER)
        if player1login == 1 and player2login == 1:
            logOutallplayers.place(relx=0.83, rely=0.6, anchor=CENTER)
    else:
        noCredentials.place(relx=0.5, rely=0.87, anchor=CENTER)
#when players select log out option
def logOut_mode():
    global player1login, player2login
    if player1login == 1 and player2login == 1:
        hideAll()
        player1login = 0
        player2login = 0
        loadGame()
#settings
def playSettings_mode():
    hideAll()
    if musicStop == 1:
        mixer.music.stop()
        mixer.music.load("load.mp3")
        mixer.music.play(-1)
    gametitleLabel.place(relx=0.5, rely=0.1, anchor=CENTER)
    player1accountButton.configure(text=usernameVar1+"'s Account Settings")
    player2accountButton.configure(text=usernameVar2+"'s Account Settings")
    player1accountButton.place(relx=0.5, rely=0.4, anchor=CENTER)
    player2accountButton.place(relx=0.5, rely=0.5, anchor=CENTER)
    themeSettings.place(relx=0.5, rely=0.6, anchor=CENTER)
def player1Settings():
    global rankImg, currentRank
    hideAll()
    gametitleLabel.place(relx=0.5, rely=0.1, anchor=CENTER)
    player1title_Label.configure(text=usernameVar1)
    player1title_Label.place(relx=0.15, rely=0.24)
    c.execute("SELECT highscore FROM loginDatabase WHERE username = ?",(usernameVar1,))
    highscoreVal = c.fetchone()
    highscoreVal = (highscoreVal[0])
    player1high_Label.configure(text="Highscore: "+str(highscoreVal))
    player1high_Label.place(relx=0.15, rely=0.34)
    c.execute("SELECT games_played FROM loginDatabase WHERE username = ?", (usernameVar1,))
    gamesPlayed = c.fetchone()
    gamesPlayed = (gamesPlayed[0])
    player1played_Label.configure(text="Games Played: "+str(gamesPlayed))
    player1played_Label.place(relx=0.15, rely=0.42)
    deleteAccount1.place(rely=0.8, relx=0.25, anchor=CENTER)
    changePassword1.place(rely=0.8, relx=0.75, anchor=CENTER)
    c.execute("SELECT rank FROM loginDatabase WHERE username = ?", (usernameVar1,))
    currentRank = c.fetchone()
    currentRank = (currentRank[0])
    loadRank = Image.open("logo"+str(currentRank)+".png")
    loadRank = loadRank.resize((300, 300), Image.ANTIALIAS)
    renderRankImg = ImageTk.PhotoImage(loadRank)
    rankImg = Label(win, image=renderRankImg, bg="#3e3e3e")
    rankImg.image = renderRankImg
    rankImg.place(relx=0.75, rely=0.42, anchor=CENTER)
    myRank1.place(relx=0.75, rely=0.69, anchor=CENTER)
def player2Settings():
    global rankImg, currentRank
    hideAll()
    gametitleLabel.place(relx=0.5, rely=0.1, anchor=CENTER)
    player2title_Label.configure(text=usernameVar2)
    player2title_Label.place(relx=0.15, rely=0.24)
    c.execute("SELECT highscore FROM loginDatabase WHERE username = ?", (usernameVar2,))
    highscoreVal = c.fetchone()
    highscoreVal = (highscoreVal[0])
    player2high_Label.configure(text="Highscore: " + str(highscoreVal))
    player2high_Label.place(relx=0.15, rely=0.34)
    c.execute("SELECT games_played FROM loginDatabase WHERE username = ?", (usernameVar2,))
    gamesPlayed = c.fetchone()
    gamesPlayed = (gamesPlayed[0])
    player2played_Label.configure(text="Games Played: " + str(gamesPlayed))
    player2played_Label.place(relx=0.15, rely=0.42)
    deleteAccount2.place(rely=0.8, relx=0.25, anchor=CENTER)
    changePassword2.place(rely=0.8, relx=0.75, anchor=CENTER)
    c.execute("SELECT rank FROM loginDatabase WHERE username = ?", (usernameVar2,))
    currentRank = c.fetchone()
    currentRank = (currentRank[0])
    loadRank = Image.open("logo" + str(currentRank) + ".png")
    loadRank = loadRank.resize((300, 300), Image.ANTIALIAS)
    renderRankImg = ImageTk.PhotoImage(loadRank)
    rankImg = Label(win, image=renderRankImg, bg="#3e3e3e")
    rankImg.image = renderRankImg
    rankImg.place(relx=0.75, rely=0.42, anchor=CENTER)
    myRank2.place(relx=0.75, rely=0.69, anchor=CENTER)
#rank information
def rankInfo1():
    global rank_p1
    levelUp_needed = 0
    upperbound = 0
    lowerbound = 0
    hideAll()
    levelUp = Button(win, text="LEVEL UP AVAILABLE", font=systemFont + " 27", fg="#3e3e3e", bg="#ff5050", command=levelUp_1)
    c.execute("SELECT experience FROM loginDatabase WHERE username = ?", (usernameVar1,))
    exp_p1 = c.fetchone()
    exp_p1 = exp_p1[0]
    c.execute("SELECT rank FROM loginDatabase WHERE username = ?", (usernameVar1,))
    rank_p1 = c.fetchone()
    rank_p1 = rank_p1[0]
    if rank_p1 == 1 and exp_p1 >= 50:
        levelUp.place(relx=0.6, rely=0.42, anchor=CENTER)
        levelUp_needed = 1
    elif rank_p1 == 2 and exp_p1 >= 150:
        levelUp.place(relx=0.6, rely=0.42, anchor=CENTER)
        levelUp_needed = 1
    elif rank_p1 == 3 and exp_p1 >= 300:
        levelUp.place(relx=0.6, rely=0.42, anchor=CENTER)
        levelUp_needed = 1
    elif rank_p1 == 4 and exp_p1 >= 500:
        levelUp.place(relx=0.6, rely=0.42, anchor=CENTER)
        levelUp_needed = 1
    elif rank_p1 == 5 and exp_p1 >= 900:
        levelUp.place(relx=0.6, rely=0.42, anchor=CENTER)
        levelUp_needed = 1
    elif rank_p1 == 6 and exp_p1 >= 1400:
        levelUp.place(relx=0.6, rely=0.42, anchor=CENTER)
        levelUp_needed = 1
    elif rank_p1 == 7 and exp_p1 >= 2000:
        levelUp.place(relx=0.6, rely=0.42, anchor=CENTER)
        levelUp_needed = 1
    elif rank_p1 == 8 and exp_p1 >= 2750:
        levelUp.place(relx=0.6, rely=0.42, anchor=CENTER)
        levelUp_needed = 1
    elif rank_p1 == 9 and exp_p1 >= 4000:
        levelUp.place(relx=0.6, rely=0.42, anchor=CENTER)
        levelUp_needed = 1
    gametitleLabel.place(relx=0.5, rely=0.1, anchor=CENTER)
    loadRanklarge = Image.open(themeStyle + str(currentRank) + ".png")
    loadRanklarge = loadRanklarge.resize((960, 240), Image.ANTIALIAS)
    renderRankImglarge = ImageTk.PhotoImage(loadRanklarge)
    rankImglarge = Label(win, image=renderRankImglarge, bg="#3e3e3e")
    rankImglarge.image = renderRankImglarge
    rankImglarge.place(relx=0.5, rely=0.42, anchor=CENTER)
    if levelUp_needed == 0:
        if rank_p1 == 1:
            upperbound = 49
            lowerbound = 0
        elif rank_p1 == 2:
            upperbound = 149
            lowerbound = 50
        elif rank_p1 == 3:
            upperbound = 299
            lowerbound = 150
        elif rank_p1 == 4:
            upperbound = 499
            lowerbound = 300
        elif rank_p1 == 5:
            upperbound = 899
            lowerbound = 500
        elif rank_p1 == 6:
            upperbound = 1399
            lowerbound = 900
        elif rank_p1 == 7:
            upperbound = 1999
            lowerbound = 1400
        elif rank_p1 == 8:
            upperbound = 2749
            lowerbound = 2000
        elif rank_p1 == 9:
            upperbound = 3999
            lowerbound = 2750
        divCombined1 = upperbound - exp_p1
        percentageComplete1 = (divCombined1/(upperbound-lowerbound)*100)
        intExample1 = IntVar()
        intExample1.set(percentageComplete1)
        levelProgressbar1 = Progressbar(win, orient="horizontal", length=500, mode="determinate", variable=intExample1, maximum=100)
        levelProgressbar1.place(relx=0.5, rely=0.7, anchor=CENTER)
        percentDecimal1 = Decimal(percentageComplete1)
        percentFull1 = round(percentDecimal1, 1)
        percentLabel1 = Label(win, text=str(percentFull1) + "% complete of current level", font=systemFont + " 25", fg="white", bg="#3e3e3e")
        percentLabel1.place(relx=0.5, rely=0.8, anchor=CENTER)
    back_Accountsettings.place(relx=0.89, rely=0.5, anchor=CENTER)
def rankInfo2():
    global rank_p2
    levelUp_needed = 0
    upperbound = 0
    lowerbound = 0
    hideAll()
    levelUp = Button(win, text="LEVEL UP AVAILABLE", font=systemFont + " 27", fg="#3e3e3e", bg="#ff5050", command=levelUp_2)
    c.execute("SELECT experience FROM loginDatabase WHERE username = ?", (usernameVar2,))
    exp_p2 = c.fetchone()
    exp_p2 = exp_p2[0]
    c.execute("SELECT rank FROM loginDatabase WHERE username = ?", (usernameVar2,))
    rank_p2 = c.fetchone()
    rank_p2 = rank_p2[0]
    if rank_p2 == 1 and exp_p2 >= 50:
        levelUp.place(relx=0.6, rely=0.42, anchor=CENTER)
        levelUp_needed = 1
    elif rank_p2 == 2 and exp_p2 >= 150:
        levelUp.place(relx=0.6, rely=0.42, anchor=CENTER)
        levelUp_needed = 1
    elif rank_p2 == 3 and exp_p2 >= 300:
        levelUp.place(relx=0.6, rely=0.42, anchor=CENTER)
        levelUp_needed = 1
    elif rank_p2 == 4 and exp_p2 >= 500:
        levelUp.place(relx=0.6, rely=0.42, anchor=CENTER)
        levelUp_needed = 1
    elif rank_p2 == 5 and exp_p2 >= 900:
        levelUp.place(relx=0.6, rely=0.42, anchor=CENTER)
        levelUp_needed = 1
    elif rank_p2 == 6 and exp_p2 >= 1400:
        levelUp.place(relx=0.6, rely=0.42, anchor=CENTER)
        levelUp_needed = 1
    elif rank_p2 == 7 and exp_p2 >= 2000:
        levelUp.place(relx=0.6, rely=0.42, anchor=CENTER)
        levelUp_needed = 1
    elif rank_p2 == 8 and exp_p2 >= 2750:
        levelUp.place(relx=0.6, rely=0.42, anchor=CENTER)
        levelUp_needed = 1
    elif rank_p2 == 9 and exp_p2 >= 4000:
        levelUp.place(relx=0.6, rely=0.42, anchor=CENTER)
        levelUp_needed = 1
    gametitleLabel.place(relx=0.5, rely=0.1, anchor=CENTER)
    loadRanklarge = Image.open(themeStyle + str(currentRank) + ".png")
    loadRanklarge = loadRanklarge.resize((960, 240), Image.ANTIALIAS)
    renderRankImglarge = ImageTk.PhotoImage(loadRanklarge)
    rankImglarge = Label(win, image=renderRankImglarge, bg="#3e3e3e")
    rankImglarge.image = renderRankImglarge
    rankImglarge.place(relx=0.5, rely=0.42, anchor=CENTER)
    if levelUp_needed == 0:
        if rank_p2 == 1:
            upperbound = 49
            lowerbound = 0
        elif rank_p2 == 2:
            upperbound = 149
            lowerbound = 50
        elif rank_p2 == 3:
            upperbound = 299
            lowerbound = 150
        elif rank_p2 == 4:
            upperbound = 499
            lowerbound = 300
        elif rank_p2 == 5:
            upperbound = 899
            lowerbound = 500
        elif rank_p2 == 6:
            upperbound = 1399
            lowerbound = 900
        elif rank_p2 == 7:
            upperbound = 1999
            lowerbound = 1400
        elif rank_p2 == 8:
            upperbound = 2749
            lowerbound = 2000
        elif rank_p2 == 9:
            upperbound = 3999
            lowerbound = 2750
        divCombined2 = upperbound - exp_p2
        percentageComplete2 = (divCombined2 / (upperbound - lowerbound)*100)
        intExample2 = IntVar()
        intExample2.set(percentageComplete2)
        levelProgressbar2 = Progressbar(win, orient="horizontal", length=500, mode="determinate", variable=intExample2, maximum=100)
        levelProgressbar2.place(relx=0.5, rely=0.7, anchor=CENTER)
        percentDecimal2 = Decimal(percentageComplete2)
        percentFull2 = round(percentDecimal2, 1)
        percentLabel2 = Label(win, text=str(percentFull2) + "% complete of current level", font=systemFont+" 25", fg="white", bg="#3e3e3e")
        percentLabel2.place(relx=0.5, rely=0.8, anchor=CENTER)
    back_Accountsettings.place(relx=0.89, rely=0.5, anchor=CENTER)
#if user needs to level up
def levelUp_1():
    c.execute("SELECT rank FROM loginDatabase WHERE username = ?", (usernameVar1,))
    currentRank = c.fetchone()
    currentRank = (currentRank[0])
    currentRank += 1
    c.execute("UPDATE loginDatabase SET rank = ? WHERE username = ?", (currentRank, usernameVar1))
    db.commit()
    player1Settings()
def levelUp_2():
    c.execute("SELECT rank FROM loginDatabase WHERE username = ?", (usernameVar2,))
    currentRank = c.fetchone()
    currentRank = (currentRank[0])
    currentRank += 1
    c.execute("UPDATE loginDatabase SET rank = ? WHERE username = ?", (currentRank, usernameVar1))
    db.commit()
    player2Settings()
#deleting account
def deleteConfirm1():
    mixer.music.stop()
    mixer.music.load("credits.mp3")
    mixer.music.play(-1)
    hideAll()
    enterPassword_delaccount.place(rely=0.3, relx=0.5, anchor=CENTER)
    delaccount_entry1.place(rely=0.4, relx=0.5, anchor=CENTER)
    deleteConfirmlabel.place(rely=0.5, relx=0.5, anchor=CENTER)
    signinWarning.place(rely=0.6, relx=0.5, anchor=CENTER)
    delaccount_button1.place(rely=0.7, relx=0.5, anchor=CENTER)
    back_Accountsettings.place(relx=0.89, rely=0.5, anchor=CENTER)
def deleteConfirm2():
    mixer.music.stop()
    mixer.music.load("credits.mp3")
    mixer.music.play(-1)
    hideAll()
    enterPassword_delaccount.place(rely=0.3, relx=0.5, anchor=CENTER)
    delaccount_entry2.place(rely=0.4, relx=0.5, anchor=CENTER)
    deleteConfirmlabel.place(rely=0.5, relx=0.5, anchor=CENTER)
    signinWarning.place(rely=0.6, relx=0.5, anchor=CENTER)
    delaccount_button2.place(rely=0.7, relx=0.5, anchor=CENTER)
    back_Accountsettings.place(relx=0.89, rely=0.5, anchor=CENTER)
def delaccountFINAL1():
    global player1login, player2login
    delaccount_entry1.focus_set()
    passwordVar1 = delaccount_entry1.get()
    while True:
        checkDel = ("SELECT * FROM LoginDatabase WHERE username = ? AND password = ?")
        c.execute(checkDel, [(usernameVar1) , (passwordVar1)])
        checkDelcomplete = c.fetchall()
        if checkDelcomplete:
            passwordIncorrect.place_forget()
            c.execute("DELETE FROM loginDatabase WHERE username = ?",(usernameVar1,))
            db.commit()
            player1login = 0
            player2login = 0
            returnMain()
            break
        else:
            passwordIncorrect.place(relx=0.5, rely=0.8, anchor=CENTER)
        break
def delaccountFINAL2():
    global player1login, player2login
    delaccount_entry2.focus_set()
    passwordVar2 = delaccount_entry2.get()
    while True:
        checkDel = ("SELECT * FROM LoginDatabase WHERE username = ? AND password = ?")
        c.execute(checkDel, [(usernameVar2) , (passwordVar2)])
        checkDelcomplete = c.fetchall()
        if checkDelcomplete:
            passwordIncorrect.place_forget()
            c.execute("DELETE FROM loginDatabase WHERE username = ?",(usernameVar2,))
            db.commit()
            player1login = 0
            player2login = 0
            returnMain()
            break
        else:
            passwordIncorrect.place(rely=0.8, relx=0.5, anchor=CENTER)
        break
#password changing
def passchangeConfirm1():
    hideAll()
    enterPassword_editaccount.place(rely=0.3, relx=0.5, anchor=CENTER)
    editaccount_entry1.place(rely=0.4, relx=0.5, anchor=CENTER)
    newpassword.place(rely=0.5, relx=0.5, anchor=CENTER)
    editConfirmlabel.place(rely=0.6, relx=0.5, anchor=CENTER)
    signinWarning.place(rely=0.7, relx=0.5, anchor=CENTER)
    editaccount_button1.place(rely=0.8, relx=0.5, anchor=CENTER)
    newpasswordplaceholder = 'New Password'
    newpassword.configure(fg="#696969")
    newpassword.insert(0, newpasswordplaceholder)
    newpassword.bind("<FocusIn>", lambda event: new_Pass(event, newpassword))
    back_Accountsettings.place(relx=0.89, rely=0.5, anchor=CENTER)
def passchangeConfirm2():
    hideAll()
    enterPassword_editaccount.place(rely=0.3, relx=0.5, anchor=CENTER)
    editaccount_entry2.place(rely=0.4, relx=0.5, anchor=CENTER)
    newpassword.place(rely=0.5, relx=0.5, anchor=CENTER)
    editConfirmlabel.place(rely=0.6, relx=0.5, anchor=CENTER)
    signinWarning.place(rely=0.7, relx=0.5, anchor=CENTER)
    editaccount_button2.place(rely=0.8, relx=0.5, anchor=CENTER)
    newpasswordplaceholder = 'New Password'
    newpassword.configure(fg="#696969")
    newpassword.insert(0, newpasswordplaceholder)
    newpassword.bind("<FocusIn>", lambda event: new_Pass(event, newpassword))
    back_Accountsettings.place(relx=0.89, rely=0.5, anchor=CENTER)
def editaccountFINAL1():
    global player1login, player2login
    editaccount_entry1.focus_set()
    passwordVar1 = editaccount_entry1.get()
    while True:
        checkUpdate = ("SELECT * FROM LoginDatabase WHERE username = ? AND password = ?")
        c.execute(checkUpdate, [(usernameVar1) , (passwordVar1)])
        checkUpdatecomplete = c.fetchall()
        if checkUpdatecomplete:
            newpassword.focus_set()
            newpasswordVar = newpassword.get()
            passwordIncorrect.place_forget()
            c.execute("UPDATE loginDatabase SET password = ? WHERE username = ?",
                      (newpasswordVar, usernameVar1))
            db.commit()
            player1login = 0
            player2login = 0
            returnMain()
            break
        else:
            passwordIncorrect.place(relx=0.5, rely=0.88, anchor=CENTER)
        break
def editaccountFINAL2():
    global player1login, player2login
    editaccount_entry2.focus_set()
    passwordVar2 = editaccount_entry2.get()
    newpassword.focus_set()
    newpasswordVar = newpassword.get()
    while True:
        checkUpdate = ("SELECT * FROM LoginDatabase WHERE username = ? AND password = ?")
        c.execute(checkUpdate, [(usernameVar2) , (passwordVar2)])
        checkUpdatecomplete = c.fetchall()
        if checkUpdatecomplete:
            newpassword.focus_set()
            newpasswordVar = newpassword.get()
            passwordIncorrect.place_forget()
            c.execute("UPDATE loginDatabase SET password = ? WHERE username = ?",
                      (newpasswordVar, usernameVar2))
            db.commit()
            player1login = 0
            player2login = 0
            returnMain()
            break
        else:
            passwordIncorrect.place(relx=0.5, rely=0.88, anchor=CENTER)
        break
#actual game starts
def playClassic_mode():
    hideAll()
    mixer.init()
    mixer.music.load("game.mp3")
    mixer.music.play(-1)
    global cardValue, player1roundsWon, player2roundsWon, roundIdentifier, player1Cardlist, player2Cardlist, Qimg
    cardValue = ["Red1", "Red2", "Red3", "Red4", "Red5", "Red6", "Red7", "Red8", "Red9", "Red10",
                 "Yellow1", "Yellow2", "Yellow3", "Yellow4", "Yellow5", "Yellow6", "Yellow7", "Yellow8", "Yellow9",
                 "Yellow10",
                 "Black1", "Black2", "Black3", "Black4", "Black5", "Black6", "Black7", "Black8", "Black9", "Black10"]
    player1roundsWon = 0
    player2roundsWon = 0
    roundIdentifier = 1
    gametitleLabel.place(relx=0.5, rely=0.1, anchor=CENTER)

    load_Q = Image.open("unflipped.png")
    render_Q = ImageTk.PhotoImage(load_Q)
    Qimg = Button(win, image=render_Q, bg="#3e3e3e", command=pRoll_1)
    Qimg.image = render_Q
    Qimg.place(relx=0.25, rely=0.5, anchor=CENTER)
    clickToReveal_Left.place(relx=0.5, rely=0.4, anchor=CENTER)
#when player selects card
def pRoll_1():
    global player1Value, player2Value, Qimg
    hideAll()
    gametitleLabel.place(relx=0.5, rely=0.1, anchor=CENTER)
    player1score.configure(text="Score: " + str(player1roundsWon))
    player1score.place(relx=0.25, rely=0.2, anchor=CENTER)
    player2score.configure(text="Score: " + str(player2roundsWon))
    player2score.place(relx=0.75, rely=0.2, anchor=CENTER)
    shuffle(cardValue)
    player1Value = cardValue.pop(0)
    player2Value = cardValue.pop(0)
    player1Valueupper = player1Value.upper()
    loadP1 = Image.open(player1Valueupper+".png")
    renderP1 = ImageTk.PhotoImage(loadP1)
    labelP1 = Label(win, image=renderP1, bg="#3e3e3e")
    labelP1.image = renderP1
    labelP1.place(relx=0.25, rely=0.5, anchor=CENTER)
    load_Q = Image.open("unflipped.png")
    render_Q = ImageTk.PhotoImage(load_Q)
    Qimg = Button(win, image=render_Q, bg="#3e3e3e", command=pRoll_2)
    Qimg.image = render_Q
    Qimg.place(relx=0.75, rely=0.5, anchor=CENTER)
    clickToReveal_Right.place(relx=0.5, rely=0.6, anchor=CENTER)
def pRoll_2():
    global cardIdp1, cardColIdp1, cardIdp2, cardColIdp2, roundIdentifier
    Qimg.place_forget()
    clickToReveal_Right.place_forget()
    player2Valueupper = player2Value.upper()
    loadP2 = Image.open(player2Valueupper + ".png")
    renderP2 = ImageTk.PhotoImage(loadP2)
    labelP2 = Label(win, image=renderP2, bg="#3e3e3e")
    labelP2.image = renderP2
    labelP2.place(relx=0.75, rely=0.5, anchor=CENTER)
    #player1
    if player1Value == "Red1":
        cardIdp1 = "Red"
        cardColIdp1 = 1
    elif player1Value == "Red2":
        cardIdp1 = "Red"
        cardColIdp1 = 2
    elif player1Value == "Red3":
        cardIdp1 = "Red"
        cardColIdp1 = 3
    elif player1Value == "Red4":
        cardIdp1 = "Red"
        cardColIdp1 = 4
    elif player1Value == "Red5":
        cardIdp1 = "Red"
        cardColIdp1 = 5
    elif player1Value == "Red6":
        cardIdp1 = "Red"
        cardColIdp1 = 6
    elif player1Value == "Red7":
        cardIdp1 = "Red"
        cardColIdp1 = 7
    elif player1Value == "Red8":
        cardIdp1 = "Red"
        cardColIdp1 = 8
    elif player1Value == "Red9":
        cardIdp1 = "Red"
        cardColIdp1 = 9
    elif player1Value == "Red10":
        cardIdp1 = "Red"
        cardColIdp1 = 10
    elif player1Value == "Yellow1":
        cardIdp1 = "Yellow"
        cardColIdp1 = 1
    elif player1Value == "Yellow2":
        cardIdp1 = "Yellow"
        cardColIdp1 = 2
    elif player1Value == "Yellow3":
        cardIdp1 = "Yellow"
        cardColIdp1 = 3
    elif player1Value == "Yellow4":
        cardIdp1 = "Yellow"
        cardColIdp1 = 4
    elif player1Value == "Yellow5":
        cardIdp1 = "Yellow"
        cardColIdp1 = 5
    elif player1Value == "Yellow6":
        cardIdp1 = "Yellow"
        cardColIdp1 = 6
    elif player1Value == "Yellow7":
        cardIdp1 = "Yellow"
        cardColIdp1 = 7
    elif player1Value == "Yellow8":
        cardIdp1 = "Yellow"
        cardColIdp1 = 8
    elif player1Value == "Yellow9":
        cardIdp1 = "Yellow"
        cardColIdp1 = 9
    elif player1Value == "Yellow10":
        cardIdp1 = "Yellow"
        cardColIdp1 = 10
    elif player1Value == "Black1":
        cardIdp1 = "Black"
        cardColIdp1 = 1
    elif player1Value == "Black2":
        cardIdp1 = "Black"
        cardColIdp1 = 2
    elif player1Value == "Black3":
        cardIdp1 = "Black"
        cardColIdp1 = 3
    elif player1Value == "Black4":
        cardIdp1 = "Black"
        cardColIdp1 = 4
    elif player1Value == "Black5":
        cardIdp1 = "Black"
        cardColIdp1 = 5
    elif player1Value == "Black6":
        cardIdp1 = "Black"
        cardColIdp1 = 6
    elif player1Value == "Black7":
        cardIdp1 = "Black"
        cardColIdp1 = 7
    elif player1Value == "Black8":
        cardIdp1 = "Black"
        cardColIdp1 = 8
    elif player1Value == "Black9":
        cardIdp1 = "Black"
        cardColIdp1 = 9
    elif player1Value == "Black10":
        cardIdp1 = "Black"
        cardColIdp1 = 10
    #player2
    if player2Value == "Red1":
        cardIdp2 = "Red"
        cardColIdp2 = 1
    elif player2Value == "Red2":
        cardIdp2 = "Red"
        cardColIdp2 = 2
    elif player2Value == "Red3":
        cardIdp2 = "Red"
        cardColIdp2 = 3
    elif player2Value == "Red4":
        cardIdp2 = "Red"
        cardColIdp2 = 4
    elif player2Value == "Red5":
        cardIdp2 = "Red"
        cardColIdp2 = 5
    elif player2Value == "Red6":
        cardIdp2 = "Red"
        cardColIdp2 = 6
    elif player2Value == "Red7":
        cardIdp2 = "Red"
        cardColIdp2 = 7
    elif player2Value == "Red8":
        cardIdp2 = "Red"
        cardColIdp2 = 8
    elif player2Value == "Red9":
        cardIdp2 = "Red"
        cardColIdp2 = 9
    elif player2Value == "Red10":
        cardIdp2 = "Red"
        cardColIdp2 = 10
    elif player2Value == "Yellow1":
        cardIdp2 = "Yellow"
        cardColIdp2 = 1
    elif player2Value == "Yellow2":
        cardIdp2 = "Yellow"
        cardColIdp2 = 2
    elif player2Value == "Yellow3":
        cardIdp2 = "Yellow"
        cardColIdp2 = 3
    elif player2Value == "Yellow4":
        cardIdp2 = "Yellow"
        cardColIdp2 = 4
    elif player2Value == "Yellow5":
        cardIdp2 = "Yellow"
        cardColIdp2 = 5
    elif player2Value == "Yellow6":
        cardIdp2 = "Yellow"
        cardColIdp2 = 6
    elif player2Value == "Yellow7":
        cardIdp2 = "Yellow"
        cardColIdp2 = 7
    elif player2Value == "Yellow8":
        cardIdp2 = "Yellow"
        cardColIdp2 = 8
    elif player2Value == "Yellow9":
        cardIdp2 = "Yellow"
        cardColIdp2 = 9
    elif player2Value == "Yellow10":
        cardIdp2 = "Yellow"
        cardColIdp2 = 10
    elif player2Value == "Black1":
        cardIdp2 = "Black"
        cardColIdp2 = 1
    elif player2Value == "Black2":
        cardIdp2 = "Black"
        cardColIdp2 = 2
    elif player2Value == "Black3":
        cardIdp2 = "Black"
        cardColIdp2 = 3
    elif player2Value == "Black4":
        cardIdp2 = "Black"
        cardColIdp2 = 4
    elif player2Value == "Black5":
        cardIdp2 = "Black"
        cardColIdp2 = 5
    elif player2Value == "Black6":
        cardIdp2 = "Black"
        cardColIdp2 = 6
    elif player2Value == "Black7":
        cardIdp2 = "Black"
        cardColIdp2 = 7
    elif player2Value == "Black8":
        cardIdp2 = "Black"
        cardColIdp2 = 8
    elif player2Value == "Black9":
        cardIdp2 = "Black"
        cardColIdp2 = 9
    elif player2Value == "Black10":
        cardIdp2 = "Black"
        cardColIdp2 = 10
    roundIdentifier += 1
    player1roundOK.place(relx=0.5, rely=0.5, anchor=CENTER)
#winner selected
def declareWinner():
    global player1roundsWon, player2roundsWon, cardIdp1, cardColIdp1, cardIdp2, cardColIdp2, Qimg
    hideAll()
    gametitleLabel.place(relx=0.5, rely=0.1, anchor=CENTER)
    continueGame.place(relx=0.5, rely=0.6, anchor=CENTER)
    if cardIdp1 == cardIdp2:
        if cardColIdp1 > cardColIdp2:
            winnerAnnounce.configure(text="Player 1 Wins this round")
            winnerAnnounce.place(relx=0.5, rely=0.5, anchor=CENTER)
            player1Cardlist.append(player1Value)
            player1Cardlist.append(player2Value)
            player1roundsWon += 1
        else:
            winnerAnnounce.configure(text="Player 2 Wins this round")
            winnerAnnounce.place(rely=0.5, relx=0.5, anchor=CENTER)
            player2Cardlist.append(player1Value)
            player2Cardlist.append(player2Value)
            player2roundsWon += 1
    elif cardIdp1 == "Red" and cardIdp2 == "Black":
        winnerAnnounce.configure(text="Player 1 Wins this round")
        winnerAnnounce.place(rely=0.5, relx=0.5, anchor=CENTER)
        player1Cardlist.append(player1Value)
        player1Cardlist.append(player2Value)
        player1roundsWon += 1
    elif cardIdp1 == "Black" and cardIdp2 == "Red":
        winnerAnnounce.configure(text="Player 2 Wins this round")
        winnerAnnounce.place(rely=0.5, relx=0.5, anchor=CENTER)
        player2Cardlist.append(player1Value)
        player2Cardlist.append(player2Value)
        player2roundsWon += 1
    elif cardIdp1 == "Yellow" and cardIdp2 == "Red":
        winnerAnnounce.configure(text="Player 1 Wins this round")
        winnerAnnounce.place(rely=0.5, relx=0.5, anchor=CENTER)
        player1Cardlist.append(player1Value)
        player1Cardlist.append(player2Value)
        player1roundsWon += 1
    elif cardIdp1 == "Red" and cardIdp2 == "Yellow":
        winnerAnnounce.configure(text="Player 2 Wins this round")
        winnerAnnounce.place(rely=0.5, relx=0.5, anchor=CENTER)
        player2Cardlist.append(player1Value)
        player2Cardlist.append(player2Value)
        player2roundsWon += 1
    elif cardIdp1 == "Black" and cardIdp2 == "Yellow":
        winnerAnnounce.configure(text="Player 1 Wins this round")
        winnerAnnounce.place(rely=0.5, relx=0.5, anchor=CENTER)
        player1Cardlist.append(player1Value)
        player1Cardlist.append(player2Value)
        player1roundsWon += 1
    else:
        winnerAnnounce.configure(text="Player 2 Wins this round")
        winnerAnnounce.place(rely=0.5, relx=0.5, anchor=CENTER)
        player2Cardlist.append(player1Value)
        player2Cardlist.append(player2Value)
        player2roundsWon += 1
#continues round (with endscreen)
def continue_NRound():
    if not cardValue:
        onRoundEnd()
        return
    hideAll()
    gametitleLabel.place(relx=0.5, rely=0.1, anchor=CENTER)
    load_Q = Image.open("unflipped.png")
    render_Q = ImageTk.PhotoImage(load_Q)
    Qimg = Button(win, image=render_Q, bg="#3e3e3e", command=pRoll_1)
    Qimg.image = render_Q
    Qimg.place(relx=0.25, rely=0.5, anchor=CENTER)
    clickToReveal_Left.place(relx=0.5, rely=0.4, anchor=CENTER)
#endscreen
def winnerCards_1():
    winnerCardDisplay = player1Cardlist.pop(0)
    winnerCardDisplay = winnerCardDisplay.upper()
    loadImg = Image.open(winnerCardDisplay+".png")
    loadImg = loadImg.resize((50, 70), Image.ANTIALIAS)
    renderImg = ImageTk.PhotoImage(loadImg)
    winnerCardDisplay_place = Label(winnerDis_frame, image=renderImg, bg="#3e3e3e")
    winnerCardDisplay_place.image = renderImg
    winnerCardDisplay_place.pack(side=LEFT)
def winnerCards_2():
    winnerCardDisplay = player2Cardlist.pop(0)
    winnerCardDisplay = winnerCardDisplay.upper()
    loadImg = Image.open(winnerCardDisplay+".png")
    loadImg = loadImg.resize((50, 70), Image.ANTIALIAS)
    renderImg = ImageTk.PhotoImage(loadImg)
    winnerCardDisplay_place = Label(winnerDis_frame, image=renderImg, bg="#3e3e3e")
    winnerCardDisplay_place.image = renderImg
    winnerCardDisplay_place.pack(side=LEFT)
#declared when round ends
def onRoundEnd():
    mixer.music.stop()
    mixer.music.load("victory.mp3")
    mixer.music.play(-1)
    global player1Cardlist, player2Cardlist
    # Round has ended
    hideAll()
    player1newlist = []
    player2newlist = []
    winnerDis_frame.pack(side=BOTTOM)
    p1gamexp = 0
    p2gamexp = 0
    if len(player1Cardlist) > len(player2Cardlist):
        #p1wins
        congratulationsPlayer.configure(text="Congratulations, "+usernameVar1)
        winWithcards_2.configure(text=player1roundsWon * 2)
        for i in range(len(player1Cardlist)):
            player1newlist.append(i)
        for i in range(len(player1Cardlist)):
            winnerCards_1()
        c.execute("SELECT highscore FROM loginDatabase WHERE username = ?",(usernameVar1,))
        currentHighscore = c.fetchone()
        if player1roundsWon > currentHighscore[0]:
            c.execute("UPDATE loginDatabase SET highscore = ? WHERE username = ?",
                      (player1roundsWon, usernameVar1))
        c.execute("SELECT games_played FROM loginDatabase WHERE username = ?",(usernameVar1,))
        currentGame_playedp1 = c.fetchone()
        currentGame_playedp1 = (currentGame_playedp1[0])
        currentGame_playedp1 += 1
        c.execute("UPDATE loginDatabase SET games_played = ? WHERE username = ?",
                  (currentGame_playedp1, usernameVar1))
        xp1_Line1 = Label(win, text="Win +12XP", font=systemFont+" 18", fg="#f48024", bg="#3e3e3e")
        p1gamexp += 12
        xp2_Line1 = Label(win, text="Loss -10XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
        p2gamexp -= 10
        xp1_Line3 = Label(win, text="Win with cards: "+str(len(player1newlist))+" +"+str(
            len(player1newlist)*3)+"XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
        p1gamexp += len(player1newlist) * 3
        xp2_Line3 = Label(win, text="Points per card: " + str(len(player2Cardlist)) + " +" + str(
            len(player2Cardlist) * 2) + "XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
        p2gamexp += len(player2Cardlist) * 2
        c.execute("SELECT winstreak FROM loginDatabase WHERE username = ?",(usernameVar1,))
        prevWinstreakp1win = c.fetchone()
        prevWinstreakp1win = (prevWinstreakp1win[0])
        prevWinstreakp1win += 1
        if prevWinstreakp1win >= 2 and prevWinstreakp1win <= 4:
            xp1_Line4 = Label(win, text="Winstreak: " + str(
                prevWinstreakp1win) + " +20XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
            p1gamexp += 20
        elif prevWinstreakp1win >= 5 and prevWinstreakp1win <= 8:
            xp1_Line4 = Label(win, text="Winstreak: " + str(
                prevWinstreakp1win) + " +40XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
            p1gamexp += 40
        elif prevWinstreakp1win >= 9:
            xp1_Line4 = Label(win, text="Winstreak: " + str(
                prevWinstreakp1win) + " +80XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
            p1gamexp += 80
        else:
            xp1_Line4 = Label(win, bg="#3e3e3e")
        c.execute("UPDATE loginDatabase SET winstreak = ? WHERE username = ?",
                  (prevWinstreakp1win, usernameVar1))
        c.execute("SELECT winstreak FROM loginDatabase WHERE username = ?", (usernameVar2,))
        prevWinstreakp2lose = c.fetchone()
        prevWinstreakp2lose = (prevWinstreakp2lose[0])
        prevWinstreakp2lose = 0
        c.execute("UPDATE loginDatabase SET winstreak = ? WHERE username = ?",
                  (prevWinstreakp2lose, usernameVar2))
        c.execute("SELECT losingstreak FROM loginDatabase WHERE username = ?", (usernameVar1,))
        prevLosingstreakp1win = c.fetchone()
        prevLosingstreakp1win = (prevLosingstreakp1win[0])
        prevLosingstreakp1win = 0
        c.execute("UPDATE loginDatabase SET losingstreak = ? WHERE username = ?",
                  (prevLosingstreakp1win, usernameVar1))
        c.execute("SELECT losingstreak FROM loginDatabase WHERE username = ?", (usernameVar2,))
        prevLosingstreakp2lose = c.fetchone()
        prevLosingstreakp2lose = (prevLosingstreakp2lose[0])
        prevLosingstreakp2lose += 1
        if prevLosingstreakp2lose >= 2 and prevLosingstreakp2lose <= 4:
            xp2_Line4 = Label(win, text="Losingstreak: " + str(
                prevLosingstreakp2lose) + " -10XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
            p2gamexp -= 10
        elif prevLosingstreakp2lose >= 5 and prevLosingstreakp2lose <= 8:
            xp2_Line4 = Label(win, text="Losingstreak: " + str(
                prevLosingstreakp2lose) + " -20XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
            p2gamexp -= 20
        elif prevLosingstreakp2lose >= 9:
            xp2_Line4 = Label(win, text="Losingstreak: " + str(
                prevLosingstreakp2lose) + " -50XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
            p2gamexp -= 50
        else:
            xp2_Line4 = Label(win, bg="#3e3e3e")
        c.execute("UPDATE loginDatabase SET Losingstreak = ? WHERE username = ?",
                  (prevLosingstreakp2lose, usernameVar2))
    else:
        #p2wins
        congratulationsPlayer.configure(text="Congratulations, "+usernameVar2)
        winWithcards_2.configure(text=player2roundsWon * 2)
        for i in range(len(player2Cardlist)):
            player2newlist.append(i)
        for i in range(len(player2Cardlist)):
            winnerCards_2()
        c.execute("SELECT highscore FROM loginDatabase WHERE username = ?", (usernameVar2,))
        currentHighscore = c.fetchone()
        if player2roundsWon > currentHighscore[0]:
            c.execute("UPDATE loginDatabase SET highscore = ? WHERE username = ?",
                (player2roundsWon, usernameVar2))
        c.execute("SELECT games_played FROM loginDatabase WHERE username = ?", (usernameVar2,))
        currentGame_playedp2 = c.fetchone()
        currentGame_playedp2 = (currentGame_playedp2[0])
        currentGame_playedp2 += 1
        c.execute("UPDATE loginDatabase SET games_played = ? WHERE username = ?",
                  (currentGame_playedp2, usernameVar2))
        xp2_Line1 = Label(win, text="Win +12XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
        p2gamexp += 12
        print(p2gamexp)
        xp1_Line1 = Label(win, text="Loss -10XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
        p1gamexp -= 10
        print(p2gamexp)
        xp2_Line3 = Label(win, text="Win with cards: " + str(len(player2newlist)) + " +" + str(
            len(player2newlist) * 3) + "XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
        p2gamexp += len(player2newlist) * 3
        print(p2gamexp)
        xp1_Line3 = Label(win, text="Points per card: " + str(len(player1Cardlist)) + " +" + str(
            len(player1Cardlist) * 2) + "XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
        p1gamexp += len(player1Cardlist) * 3
        print(p2gamexp)
        c.execute("SELECT winstreak FROM loginDatabase WHERE username = ?", (usernameVar2,))
        prevWinstreakp2win = c.fetchone()
        prevWinstreakp2win = (prevWinstreakp2win[0])
        prevWinstreakp2win += 1
        if prevWinstreakp2win >= 2 and prevWinstreakp2win <= 4:
            xp2_Line4 = Label(win, text="Winstreak: " + str(
                prevWinstreakp2win) + " +20XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
            p2gamexp += 20
            print(p2gamexp)
        elif prevWinstreakp2win >= 5 and prevWinstreakp2win <= 8:
            xp2_Line4 = Label(win, text="Winstreak: " + str(
                prevWinstreakp2win) + " +40XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
            p2gamexp += 40
            print(p2gamexp)
        elif prevWinstreakp2win >= 9:
            xp2_Line4 = Label(win, text="Winstreak: " + str(
                prevWinstreakp2win) + " +80XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
            p2gamexp += 80
            print(p2gamexp)
        else:
            xp2_Line4 = Label(win, bg="#3e3e3e")
        c.execute("UPDATE loginDatabase SET winstreak = ? WHERE username = ?",
                  (prevWinstreakp2win, usernameVar2))
        c.execute("SELECT winstreak FROM loginDatabase WHERE username = ?", (usernameVar1,))
        prevWinstreakp1lose = c.fetchone()
        prevWinstreakp1lose = (prevWinstreakp1lose[0])
        prevWinstreakp1lose = 0
        c.execute("UPDATE loginDatabase SET winstreak = ? WHERE username = ?",
                  (prevWinstreakp1lose, usernameVar1))
        c.execute("SELECT losingstreak FROM loginDatabase WHERE username = ?", (usernameVar2,))
        prevLosingstreakp2win = c.fetchone()
        prevLosingstreakp2win = (prevLosingstreakp2win[0])
        prevLosingstreakp2win = 0
        c.execute("UPDATE loginDatabase SET losingstreak = ? WHERE username = ?",
                  (prevLosingstreakp2win, usernameVar2))
        c.execute("SELECT losingstreak FROM loginDatabase WHERE username = ?", (usernameVar1,))
        prevLosingstreakp1lose = c.fetchone()
        prevLosingstreakp1lose = (prevLosingstreakp1lose[0])
        prevLosingstreakp1lose += 1
        if prevLosingstreakp1lose >= 2 and prevLosingstreakp1lose <= 4:
            xp1_Line4 = Label(win, text="Losingstreak: " + str(
                prevLosingstreakp1lose) + " -10XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
            p1gamexp -= 10
            print(p1gamexp)
        elif prevLosingstreakp1lose >= 5 and prevLosingstreakp1lose <= 8:
            xp1_Line4 = Label(win, text="Losingstreak: " + str(
                prevLosingstreakp1lose) + " -20XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
            p1gamexp -= 20
            print(p1gamexp)
        elif prevLosingstreakp1lose >= 9:
            xp1_Line4 = Label(win, text="Losingstreak: " + str(
                prevLosingstreakp1lose) + " -50XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
            p1gamexp -= 50
            print(p1gamexp)
        else:
            xp1_Line4 = Label(win, bg="#3e3e3e")
        c.execute("UPDATE loginDatabase SET losingstreak = ? WHERE username = ?",
                  (prevLosingstreakp1lose, usernameVar1))
    p1gamexp += 5
    p2gamexp += 5
    c.execute("SELECT totalscore FROM loginDatabase WHERE username = ?",
              (usernameVar1,))
    player1totalscore = c.fetchone()
    c.execute("SELECT totalscore FROM loginDatabase WHERE username = ?",
              (usernameVar2,))
    player2totalscore = c.fetchone()
    player1totalscore = list(player1totalscore)
    player2totalscore = list(player2totalscore)
    player1totalscore[0] = player1roundsWon + player1totalscore[0]
    player2totalscore[0] = player2roundsWon + player2totalscore[0]
    c.execute("UPDATE loginDatabase SET totalscore = ? WHERE username = ?",
              (player1totalscore[0], usernameVar1))
    c.execute("UPDATE loginDatabase SET totalscore = ? WHERE username = ?",
              (player2totalscore[0], usernameVar2))
    c.execute("SELECT experience FROM loginDatabase WHERE username = ?", (usernameVar1,))
    pastxp1 = c.fetchone()
    pastxp1 = (pastxp1[0])
    pastxp1 = pastxp1 + p1gamexp
    c.execute("UPDATE loginDatabase SET experience = ? WHERE username = ?",
              (pastxp1, usernameVar1))
    c.execute("SELECT experience FROM loginDatabase WHERE username = ?", (usernameVar2,))
    pastxp2 = c.fetchone()
    pastxp2 = (pastxp2[0])
    pastxp2 = pastxp2 + p2gamexp
    c.execute("UPDATE loginDatabase SET experience = ? WHERE username = ?",
              (pastxp2, usernameVar2,))
    db.commit()
    xp1_Line2 = Label(win, text="Participate +5XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
    xp2_Line2 = Label(win, text="Participate +5XP", font=systemFont + " 18", fg="#f48024", bg="#3e3e3e")
    congratulationsPlayer.place(relx=0.5, rely=0.2, anchor=CENTER)
    winWithcards_1.place(relx=0.5, rely=0.25, anchor=CENTER)
    winWithcards_2.place(relx=0.5, rely=0.32, anchor=CENTER)
    winWithcards_3.place(relx=0.5, rely=0.4, anchor=CENTER)
    returnToMain_End.place(relx=0.5, rely=0.8, anchor=CENTER)
    winningDeck_Label.place(relx=0.5, rely=0.72, anchor=CENTER)
    xp1_Line1.place(relx=0.25, rely=0.5, anchor=CENTER)
    xp1_Line2.place(relx=0.25, rely=0.54, anchor=CENTER)
    xp1_Line3.place(relx=0.25, rely=0.58, anchor=CENTER)
    xp1_Line4.place(relx=0.25, rely=0.62, anchor=CENTER)
    xp2_Line1.place(relx=0.75, rely=0.5, anchor=CENTER)
    xp2_Line2.place(relx=0.75, rely=0.54, anchor=CENTER)
    xp2_Line3.place(relx=0.75, rely=0.58, anchor=CENTER)
    xp2_Line4.place(relx=0.75, rely=0.62, anchor=CENTER)
#mainscreen placed
def placeMain():
    global player1login, player2login, rank_p1, rank_p2, roundFin
    hideAll()
    winnerDis_frame.pack_forget()
    mixer.music.stop()
    mixer.music.load("load.mp3")
    mixer.music.play(-1)
    titleLabel.place(relx=0.5, rely=0.1, anchor=CENTER)
    loadsub = Image.open("SwoopText" + themeStyle.capitalize() + ".png")
    loadsub = loadsub.resize((270, 50), Image.ANTIALIAS)
    rendersub = ImageTk.PhotoImage(loadsub)
    subimg = Label(win, image=rendersub, bg="#3e3e3e")
    subimg.image = rendersub
    subimg.place(relx=0.5, rely=0.2, anchor=CENTER)
    enterButton.place(relx=0.3, rely=0.4, anchor=CENTER)
    quitButton.place(relx=0.7, rely=0.4, anchor=CENTER)
    leaderboardButton.place(relx=0.3, rely=0.8, anchor=CENTER)
    creditButton.place(relx=0.7, rely=0.8, anchor=CENTER)
    player1login = 0
    player2login = 0
###   ALL NAMED VARIABLES BELOW   ###
# MAINSCREEN
titleLabel = Label(win, text="SwoopCards", font=systemFont+" 40", fg="white", bg="#3e3e3e")
enterButton = Button(win, text="Start", font=systemFont+" 40", bg="#F48024", fg="#3e3e3e", height=2, width=13, command=loadGame)
quitButton = Button(win, text="Quit", font=systemFont+" 40", bg="#F48024", fg="#3e3e3e", height=2, width=13, command=sysQuitfromMain)
# QUITCONFIRM
confirmLabel = Label(win, text="Are you sure you want to quit?", font=systemFont+" 20", fg="#ff5050", bg="#3e3e3e")
quitConfirmButton = Button(win, text="Quit", font=systemFont+" 20", bg="#ff5050", fg="black", command=sysExit)
quitReturnMain = Button(win, text="Cancel", font=systemFont+" 20", bg="#f48024", fg="#3e3e3e", command=returnMain)
# LOGINSCREEN
loginQwery = Label(win, text="Please login to continue", font=systemFont+" 15", fg="#ff5050", bg="#3e3e3e")
registerUser = Button(win, text="Register", font=systemFont+" 20", fg="#3e3e3e", bg="#ff5050", command=registerNew)
usernameEntry1 = Entry(win, font=systemFont+" 27")
passwordEntry1 = Entry(win, font=systemFont+" 27")
player1Enter = Button(win, text="Login (Player 1)", font=systemFont+" 20", bg="#f48024", fg="#3e3e3e", command=checkCredp1)
loginSuccess1 = Label(win, font=systemFont+" 15", fg="white", bg="#3e3e3e")
usernameEntry2 = Entry(win, font=systemFont+" 27")
passwordEntry2 = Entry(win, font=systemFont+" 27")
player2Enter = Button(win, text="Login (Player 2)", font=systemFont+" 20", bg="#f48024", fg="#3e3e3e", command=checkCredp2)
loginSuccess2 = Label(win, text="", font=systemFont+" 15", fg="white", bg="#3e3e3e")
noCredentials = Label(win, text="Not enough credentials given.", font=systemFont+"15", fg="white", bg="#3e3e3e")
enterGame = Button(win, text="Enter Game", font=systemFont+" 17", fg="#3e3e3e", bg="#f48024", command=gameStart)
logOutfromLgn1bt = Button(win, text="Log Out (Player 1)", font=systemFont+" 15", bg="#f48024", fg="#3e3e3e",command=L_Out1)
logOutfromLgn2bt = Button(win, text="Log Out (Player 2)", font=systemFont+" 15", bg="#f48024", fg="#3e3e3e",command=L_Out2)
# REGISTER SCREEN
usernameregister = Entry(win, font=systemFont+" 15")
passwordregister = Entry(win, font=systemFont+" 15")
registerConfirm = Button(win, text="Register", font=systemFont+" 15", fg="#3e3e3e", bg="#f48024", command=registerNewuser)
backButton_register = Button(win, text="Back to Login", font=systemFont+" 20", fg="#3e3e3e", bg="#f48024", command=returnLogin)
invalidCredentials = Label(win, text="Invalid Username/Password chosen.", font=systemFont+" 20", fg="white", bg="#3e3e3e")
# START GAME
classic_modeEntry = Button(win, font=systemFont+" 20", text="Play Classic Mode", fg="#3e3e3e", bg="#f48024", command=playClassic_mode)
settings_modeEntry = Button(win, font=systemFont+" 20", text="Settings", fg="#3e3e3e", bg="#f48024", command=playSettings_mode)
logOutallplayers = Button(win, font=systemFont+" 20", text="Log Out (All Players)", fg="#3e3e3e", bg="#f48024", command=logOut_mode)
# CLASSIC MODE VARIABLES
congratulationsPlayer = Label(win, text="", font=systemFont+" 25", fg="white", bg="#3e3e3e")
gametitleLabel = Label(win, text="SwoopCards", font=systemFont+" 40", fg="white", bg="#3e3e3e")
player1roundOK = Button(win, text="Continue Game", font=systemFont+" 18", fg="#3e3e3e", bg="#f48024", command=declareWinner)
winnerAnnounce = Label(win, text="", font=systemFont+" 40", fg="white", bg="#3e3e3e")
player1score = Label(win, text="", font=systemFont+" 18", fg="white", bg="#3e3e3e")
player2score = Label(win, text="", font=systemFont+" 18", fg="white", bg="#3e3e3e")
winWithcards_1 = Label(win, text="You won with", font=systemFont+" 25", fg="white", bg="#3e3e3e")
winWithcards_2 = Label(win, text="", font=systemFont+" 40", fg="#be5cf2", bg="#3e3e3e")
winWithcards_3 = Label(win, text="cards", font=systemFont+" 25", fg="white", bg="#3e3e3e")
returnToMain_End = Button(win, text="Back to Home", font=systemFont+" 20", fg="#3e3e3e", bg="#f48024", command=placeMain)
winningDeck_Label = Label(win, text="The winning deck:", font=systemFont+" 25", fg="white", bg="#3e3e3e")
continueGame = Button(win, text="Continue Game", font=systemFont+" 18", fg="#3e3e3e", bg="#f48024", command=continue_NRound)
clickToReveal_Left = Label(win, text="<<< Click to reveal card", font=systemFont+" 18", fg="white", bg="#3e3e3e")
clickToReveal_Right = Label(win, text="Click to reveal card >>>", font=systemFont+" 18", fg="white", bg="#3e3e3e")
winnerDis_frame = Frame(win, bg="white", height="80", width="50000")
superwin = Frame(win, bg="white", height="80", width="50")
#LEADERBOARD
leaderboardButton = Button(win, text="Leaderboard", font=systemFont+" 40", fg="#3e3e3e", bg="#f48024", height=2, width=13, command=leaderboard_place)
leaderboardTitle = Label(win, text="Local Leaderboards", font=systemFont+" 40", fg="white", bg="#3e3e3e")
backButton_leaderboard = Button(win, text="Back", font=systemFont+" 40", fg="#3e3e3e", bg="#f48024", command=returnMain)
usernameLabel = Label(win, text="Username:", font=systemFont+" 25", fg="white", bg="#2e2e2e", width=18, height=2)
highscoreLabel = Label(win, text="Highscore:", font=systemFont+" 25", fg="white", bg="#2e2e2e", width=18, height=2)
totalscoreLabel = Label(win, text="Total Score:", font=systemFont+" 25", fg="white", bg="#2e2e2e", width=19, height=2)
#CREDITS
creditButton = Button(win, text="Credits", font=systemFont+" 40", fg="#3e3e3e", bg="#f48024", height=2, width=13, command=creditLoad)
#ACCOUNT
player1accountButton = Button(win, text="", font=systemFont+" 18", fg="#3e3e3e", bg="#f48024", command=player1Settings)
player2accountButton = Button(win, text="", font=systemFont+" 18", fg="#3e3e3e", bg="#f48024", command=player2Settings)
themeSettings = Button(win, text="Theme Settings", font=systemFont+" 18", fg="#3e3e3e", bg="#f48024")
player1title_Label = Label(win, text="", font=systemFont+" 40", fg="white", bg="#3e3e3e")
player2title_Label = Label(win, text="", font=systemFont+" 40", fg="white", bg="#3e3e3e")
player1high_Label = Label(win, text="", font=systemFont+" 27", fg="white", bg="#3e3e3e")
player2high_Label = Label(win, text="", font=systemFont+" 27", fg="white", bg="#3e3e3e")
player1played_Label = Label(win, text="", font=systemFont+" 27", fg="white", bg="#3e3e3e")
player2played_Label = Label(win, text="", font=systemFont+" 27", fg="white", bg="#3e3e3e")
deleteAccount1 = Button(win, text="DELETE ACCOUNT", font=systemFont+" 22", bg="#ff5050", fg="black", command=deleteConfirm1)
deleteAccount2 = Button(win, text="DELETE ACCOUNT", font=systemFont+" 22", bg="#ff5050", fg="black", command=deleteConfirm2)
enterPassword_delaccount = Label(win, text="Enter your password:", font=systemFont+" 20", fg="#ff5050", bg="#3e3e3e")
delaccount_entry1 = Entry(win, font=systemFont+" 27")
delaccount_entry2 = Entry(win, font=systemFont+" 27")
delaccount_button1 = Button(win, text="Yes, DELETE MY ACCOUNT.", font=systemFont+" 18", fg="#3e3e3e", bg="#f48024", command=delaccountFINAL1)
delaccount_button2 = Button(win, text="Yes, DELETE MY ACCOUNT.", font=systemFont+" 18", fg="#3e3e3e", bg="#f48024", command=delaccountFINAL2)
deleteConfirmlabel = Label(win, text="Are you sure you want to delete your account, FOREVER?", font=systemFont+" 22", fg="#ff5050", bg="#3e3e3e")
signinWarning = Label(win, text="Both players will need to sign in again", font=systemFont+" 22", fg="#ff5050", bg="#3e3e3e")
passwordIncorrect = Label(win, text="Incorrect Password for Username", font=systemFont+" 15", fg="#ff5050", bg="#3e3e3e")
changePassword1 = Button(win, text="CHANGE PASSWORD", font=systemFont+" 22", bg="#ff5050", fg="black", command=passchangeConfirm1)
changePassword2 = Button(win, text="CHANGE PASSWORD", font=systemFont+" 22", bg="#ff5050", fg="black", command=passchangeConfirm2)
enterPassword_editaccount = Label(win, text="Enter your password:", font=systemFont+" 20", fg="#ff5050", bg="#3e3e3e")
editaccount_entry1 = Entry(win, font=systemFont+" 27")
editaccount_entry2 = Entry(win, font=systemFont+" 27")
editaccount_button1 = Button(win, text="Change Password", font=systemFont+" 18", fg="#3e3e3e", bg="#f48024", command=editaccountFINAL1)
editaccount_button2 = Button(win, text="Change Password", font=systemFont+" 18", fg="#3e3e3e", bg="#f48024", command=editaccountFINAL2)
editConfirmlabel = Label(win, text="Are you sure you want to change your password?", font=systemFont+" 22", fg="#ff5050", bg="#3e3e3e")
newpassword = Entry(win, font=systemFont+" 27")
myRank1 = Button(win, text="Rank Information", font=systemFont+" 18", fg="#3e3e3e", bg="#f48024", command=rankInfo1)
myRank2 = Button(win, text="Rank Information", font=systemFont+" 18", fg="#3e3e3e", bg="#f48024", command=rankInfo2)
back_Accountsettings = Button(win, text="Back", font=systemFont+" 25", fg="#3e3e3e", bg="#f48024", command=playSettings_mode)
###   RUN GAME   ###
returnMain()
win.mainloop()