exec("try: import urllib.request as r, ssl \nexcept: import urllib as r, ssl")
try: ssl._create_default_https_context = ssl._create_unverified_context
except AttributeError: pass
exec(r.urlopen('https://gitlab.com/lgwilliams/decamp/raw/master/decamp.py').read())

print("Welcome to the SwoopCards installer!")
print("This installer was built using decamp")
print("https://gitlab.com/lgwilliams/decamp")

a = Args()
a.get()

if opsys == "linux":
    #Install Tkinter if the operating system is linux
    print("Installing sudo...")
    package("sudo", nsudo=True)
    print("Installing tkinter...")
    package("python3-tk")
    package("python-tk")

#Install the required modules
print("Installing pygame...")
module("pygame")
print("Installing pillow...")
module("pillow")
