# Changelog
#### This project uses semantic versioning: https://semver.org
### This is the changelog for SwoopCards
 - #### 1.0.1 - "BUG FIX 7" - 14 May, 2020
    - Fixed Typo in CHANGELOG.md

 - #### 1.0 - "RELEASE 1" - 20 Oct, 2018
    - Code condensed
    - Added feature that changes game windowsize to monitor size
    - Added comments
    
 - #### 0.11 - "SWOOPSOFT'S SWOOPCARDS" 18 Oct, 2018
    - Mute button updated
    - New logos added
    - Bug Fixes (Music)
    
 - #### 0.10 - "MUSIC AND STUFF" 17 Oct, 2018
    - Music completely done and finished - with rights
    - Placed Password Protection system, to hide password when entering.
    - Mute button added

 - #### 0.9 - "COMPETITIVE CONTENT UPDATE" 16 Oct, 2018
    - Levelling System fully complete
    - Foundations for themes and music set up

 - #### 0.8.3 - "Bug Patch *6" 08 Oct, 2018
    - Bug Fixes (Ranking System / Registering System)

 - #### 0.8.2 - "Bug Patch *5" 08 Oct, 2018
    - Bug Fixes (Ranking System)

 - #### 0.8.1 - "STATS FOR NERDS" 07 Oct, 2018
    - Basic elements of rank progression system
    - Just uploaded this to look good :)

 - #### 0.8 - "LOGIN SCREEN UI REVAMP" 04 Oct, 2018
    - UI larger and more user friendly @ login screen
    - Minor cross-platform bug fixes
    - 2 UPDATES IN 1 DAY?! NANI?!
    - 0.9 Scheduled for 10 Oct, 2018 as I am away this weekend, so no patches :/
    - 0.9 will introduce a ranking system, and will be a larger update (ALSO WE BROKE 1000 LINES THIS VERSION)

 - #### 0.7 - "ACCOUNT FEATURES UPDATE" 04 Oct, 2018
    - New Games Played Statistic, Wow!
    - Account Deletion (cough, GDPR)
    - Password Changing, for all your security needs!
    - Settings system started, in preperation for next few updates...

 - #### 0.6 - "CREDIT UPDATE" 03 Oct, 2018
    - Credits! Bask in the glory of my hard work along with legal stuff. Ugh.
    - Bug Fixes, including leaderboard and listbox fixes.

 - #### 0.5 - "COMPETITIVE CONTENT UPDATE" 30 Sep, 2018
    - LEADERBOARDS! Compete with your friends over that top spot!
    - 8 test accounts added, these can be removed by deleting the .db file, password=test
    - Menu UI Overhaul, more user friendly.
    - Function Overhaul, code broken down and better structured
    - Foundations for music development added
    - Bug Fixes, including but not limited to: developental issues, better crossplatform
    - CREDIT UPDATE (0.6) PLANNED FOR RELEASE ON THURSDAY (Smaller update, developer needs a break :/ )

 - #### 0.4 - "Basics Covered" 28 Sep, 2018
    - End screen remastered
    - Some game mechanics redone
    - New assets added
    - Logout system remastered
    - Base game 100% complete
    - LEADERBOARDS COMING SOON! (EXPECTED SUNDAY-TUESDAY in 0.5 Content Update)

 - #### 0.3.3 - "Bug Patch *4" 28 Sep, 2018
    - Code Shortened (1000 lines > 600 lines)

 - #### 0.3.2 - "Bug Patch *3" 28 Sep, 2018
    - Bug Fixes (End game bug with card placement fixed)

 - #### 0.3.1 - "Bug Patch *2" 28 Sep, 2018
    - Bug Fixes (Login System not always working)

 - #### 0.3 - "End Screen" 24 Sep, 2018
    - End screen created
    - Cross-platform issues fixed
    - Program condensed/ improved
    - IMPORTANT BUG (NOT ALL CARDS BEING PLACED ON END SCREEN)

 - #### 0.2.1 - "Bug Patch *1" 24 Sep, 2018
    - Bug Fixes (Text disappearing + Buttons not working properly + End Screen not appearing)

 - #### 0.2 - "Completed Mechanics" 23 Sep, 2018
    - Completed all basic game mechanics
    - Login system finalised
    - All files combined, old files deleted

- #### 0.1 - "Mainframe" 20 Sep, 2018
    - Mainframe for game set up
    - Added basic game mechanics
    - Foundation for TK Wrappers set up
    - Git workarounds complete
    - Changelog added (CHANGELOG.md)